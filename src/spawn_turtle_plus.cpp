//This program spawns a new turtlesim turtle by calling
// the appropriate service.
#include <ros/ros.h>
//The srv class for the service.
#include <turtlesim/Spawn.h>
#include <geometry_msgs/Twist.h>


ros::Publisher *pubPtr;

void commandVelocityReceived(
  const geometry_msgs::Twist& msgIn
) {
  geometry_msgs::Twist msgOut;
  msgOut.linear.x = -msgIn.linear.x;
  msgOut.angular.z = -msgIn.angular.z;
  pubPtr->publish(msgOut);
}

int main(int argc, char **argv){

    ros::init(argc, argv, "spawn_turtle");
    ros::NodeHandle nh;

//Create a client object for the spawn service. This
//needs to know the data type of the service and its name.
    ros::ServiceClient spawnClient
		= nh.serviceClient<turtlesim::Spawn>("spawn");

//Create the request and response objects.
    turtlesim::Spawn::Request req;
    turtlesim::Spawn::Response resp;

    req.x = 2;
    req.y = 3;
    req.theta = M_PI/2;
    req.name = "MyTurtle";

    ros::service::waitForService("spawn", ros::Duration(5));
    bool success = spawnClient.call(req,resp);

    if(success){
	ROS_INFO_STREAM("Spawned a turtle named "
			<< resp.name);
    }else{
	ROS_ERROR_STREAM("Failed to spawn.");
    }
    ros::Subscriber sub = nh.subscribe("turtle1/cmd_vel", 1000,&commandVelocityReceived);
    
    pubPtr = new ros::Publisher(nh.advertise<geometry_msgs::Twist>("MyTurtle/cmd_vel",1000));
    ros::spin();

  delete pubPtr;
}
